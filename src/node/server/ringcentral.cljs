(ns server.ringcentral
  (:require-macros
   [cljs.core.async.macros
    :refer [go go-loop alt!]])
  (:require
   [cljs.core.async :as async
    :refer [chan close! timeout put!]]
   [taoensso.timbre :as timbre]))

;; See https://www.npmjs.com/package/ringcentral

(def oauth-redirect-uri  ;; same as in config for glip, should be in env
  (aget js/process "env" "RC_OAUTH_REDIRECT"))

(def default-skype "+14154487627")

(def default-sms "+14153497189")

(def account-fax-number "+15178853065")

(defonce SDK (js/require "ringcentral"))

(def client {:id (aget js/process "env" "RC_CLIENT_ID")
             :secret (aget js/process "env" "RC_CLIENT_SECRET")})

(def REDIRECT (aget js/process "env" "REDIRECT"))

(def config
  #js{:server SDK.server.sandbox
      :redirectUri oauth-redirect-uri
      :appKey (:id client)
      :appSecret (:secret client)})

(defonce rc-sdk
  (memoize (fn [] (new SDK config))))

(defonce platform
  (memoize (fn [] (.platform (rc-sdk)))))

#_
(list (platform))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn echo [ch]
  (go-loop [data (<! ch)]
    (timbre/info "=>" data)))

(defn post [path & [payload]]
  (let [out (chan)]
    (go (-> (platform)
            (.post path
                   (clj->js payload))
            (.then (fn [response]
                     (put! out (js->clj response))
                     (close! out)))
            (.catch (fn [error]
                      (timbre/error error)
                      (close! out)))))
    out))

(defn fetch-json [path]
  (let [out (async/promise-chan)]
    (go (-> (platform)
            (.get path)
            (.then (fn [response]
                     (put! out (js->clj (.json response) :keywordize-keys true))
                     (close! out)))
            (.catch (fn [error]
                      (timbre/error error)
                      (close! out)))))
    out))

#_
(echo (fetch-json "/glip/persons/~"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn fetch-contacts []
  (fetch-json "/account/~/extension/~/address-book/contact"))

#_
(echo (fetch-contacts))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; fax

(defn send-fax [{:keys [from to text] :as payload}]
  (post "/account/~/extension/~/fax" payload))

#_
(echo (send-fax {:from "+15178853065"
                 :to "+15178853065"
                 :text "New fax..."}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn send-sms [{:keys [from to text] :as payload}]
  (-> (platform)
   (.post
    "/account/~/extension/~/sms"
    (clj->js payload))
   (.then (fn [response]
            (println "SUCCESS:" response)))
   (.catch (fn [error]
             (println "ERROR:" error)))))

(defn request-data [{:keys [webhook]}]
  {"eventFilters" ["/restapi/v1.0/glip/posts",
                   "/restapi/v1.0/glip/groups",
                   "/restapi/v1.0/subscription/~?threshold=60&interval=15"]
   "deliveryMode" {"transportType" "WebHook"
                   "address" webhook}
   "expiresIn" 604799})

(defn subscribe-events [{:keys [token webhook]}]
  (timbre/debug "SUBSCRIBE EVENTS" token webhook)
  (let []
    (-> (platform)
        (.post "/subscription" (clj->js (request-data {:webhook webhook})))
        (.then (fn [response]
                 (timbre/info "SUBSCRIBED:" response)))
        (.catch (fn [error]
                  (timbre/error "SUBSCRIBE FAILED - " error))))
    nil))

(defn logged-in []
  (let [out (async/promise-chan)]
    (-> (platform)
        (.loggedIn)
        (.then (fn [status]
                 (timbre/info "Logged In:" status)
                 (put! out status))))
    out))

(defn login [payload & {:keys [respond host]}]
  (timbre/debug "LOGIN" payload (clj->js payload))
  (go
    (if (<! (logged-in))
      (timbre/warn "Already logged in")
      (-> (platform)
          (.login (clj->js payload))
          (.then (fn [response]
                   (let [data (.json response)
                         token (.-access_token data)]
                     (timbre/debug "OAUTH:" (js->clj data))
                     (timbre/info "ACCESS TOKEN:" token)
                     (when respond
                       (respond data))
                     (subscribe-events
                      {:webhook (str "https://" host "/glip/callback")
                       :token token}))))
          (.catch (fn [error]
                    (timbre/error "Failed login - " error)
                    (when respond
                      (respond (str "Error: " error)))))))))

#_
(echo (logged-in))

(defn token-valid? []
  (.accessTokenValid (.auth (platform))))

#_
(token-valid?)

(defn oauth-handler []
  (fn [req res]
    (timbre/debug "OAUTH HANDLER:" (js->clj (.-headers req)))
    (timbre/debug "OAUTH:" req.query.code)
    (let [local-host req.headers.host #_(.-host (.-headers req))]
      (timbre/debug "HOST:" local-host)
      (if (nil? req.query.code)
        (doto res
              (.status 500)
              (.send #js{:error "Failing oauth"}))
        (login {:code req.query.code
                :redirectUri oauth-redirect-uri}
               :host local-host
               :respond #(.send res %))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def fetch-self
  (memoize (fn [& _] (fetch-json "/glip/persons/~"))))

(defn send-reply [origin content]
  (timbre/debug "SEND REPLY:" origin content)
  (let [out (chan)]
    (go
     (-> (platform)
         (.post
          "/glip/posts"
          (clj->js {:groupId (:groupId origin)
                    :text content}))
         (.then (fn [response]
                  (println "SUCCESS:" response)
                  (put! out response)
                  (close! out)))
         (.catch (fn [error]
                   (println "ERROR:" error)
                   (close! out)))))))



(defn glip-handler [dispatch]
  (fn [req res]
    (timbre/debug "GLIP CALLBACK:" (js->clj (.-headers req)))
    (if-let [token (aget (.-headers req) "validation-token")]
      (do
        (timbre/debug "Token:" token)
        (.set res "validation-token" token)
        (.send res))
      (let [event (js->clj (.-body req) :keywordize-keys true)]
        (dispatch event)
        (.send res)))))

(defn glip-webhook []
  (fn [req res]
    (timbre/debug "GLIP WEBHOOK" (js->clj (.-headers req)))
    (let [token (aget (.-headers req) "validation-token")]
      (timbre/debug "Token=" token)
      (.set res "validation-token" token)
      (.send res))))
