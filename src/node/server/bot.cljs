(ns server.bot
  (:require-macros
   [cljs.core.async.macros
    :refer [go go-loop alt!]])
  (:require
   [cljs.core.async :as async
    :refer [chan close! timeout put!]]
   [taoensso.timbre :as timbre]
   [server.ringcentral :as rc]
   [server.nlp :as nlp]))

(defmulti dispatch-message
  (fn [state message] [(:mode state)(:intent message)]))

(defmethod dispatch-message [nil :nlp/greeting]
  [state {:keys [text] :as message}]
  (go
   (rc/send-reply message "Hello from your medical office assistant bot.")
   state))

(defmethod dispatch-message :default [state {:keys [text] :as message}]
  (timbre/debug "MESSAGE TEXT:" text)
  (go
   (cond

     (or (clojure.string/starts-with? text "Find patient")
         (clojure.string/starts-with? text "find patient"))
     (let [patients (<! (rc/fetch-contacts))]
       (if patients
         (rc/send-reply message (pr-str patients))
         (rc/send-reply message "Found patient: Sheila Oberding"))
       state)

     (or (clojure.string/starts-with? text "Order")
         (clojure.string/starts-with? text "order"))
     (do
       (<! (rc/send-reply message
                          (str "What medical service should be requested?")))
       state)

     (clojure.string/starts-with? text "Massage")
     (do
       (<! (rc/send-reply message "Who is the provider of the massage therapy?"))
       state)

     (clojure.string/starts-with? text "Sheila")
     (do
       (<! (rc/send-reply message "OK. Patient ID #635."))
       (<! (rc/send-reply message "Who is the provider of the therapy?"))
       state)

     (clojure.string/starts-with? text "Saint")
     (do
       (rc/send-reply message
                      (str "Please review order for Doctor Lee:\n"
                           "Request massage therapy for Sheila Oberding\n"
                           "provided by Saint Mary's hospital.\n"
                           "Submit the order?"))
       state)

     (or (clojure.string/starts-with? text "Yes")
         (clojure.string/starts-with? text "yes"))
     (do
       (rc/send-fax {:to rc/account-fax-number
                     :from rc/account-fax-number
                     :text "Order from Doctor Lee: Requesting massage therapy for Sheila Oberding on January 18, 2018 at 3PM."})
       (rc/send-sms {:to rc/default-sms
                     :from rc/default-sms
                     :text "You are scheduled for massage therapy at Saint Mary's hospital on January 18, 2018 at 3PM."})
       (rc/send-reply message
                      (str "The order has been faxed to Saint Mary's Hospital.\n"
                           "Also, an SMS about the appointment has been sent informing the patient, Sheila Oberding."))
       state)

     :else
     (do
       (timbre/warn "No message dispatch for " text)
       (rc/send-reply message "Unknown command.")
       state))))


(defn dispatcher [incoming]
  (go-loop [state {}]
    (when-let [message (<! incoming)]
      (let [self (<! (rc/fetch-self incoming))]
        (if (= (:creatorId message) (:id self))
          (do
            (timbre/info "Ignoring message from self")
            (recur state))
          (let [message (nlp/parse-message message)]
            (timbre/debug "Intent:" message)
            (recur (<! (dispatch-message state message)))))))))

(def incoming (chan))

(dispatcher incoming)

(defn dispatch [{:keys [body] :as event}]
  (timbre/debug "DISPATCH:" event)
  (case (:type body)
      "TextMessage" (put! incoming body)
      "PrivateChat" (case (:eventType body)
                      "GroupJoined" (timbre/info "Bot joined group" body)
                      (timbre/warn "No bot event subhandler for "
                                   (:type body)(:eventType body)))
      (timbre/warn "No bot event handler for " (:type body) body)))
