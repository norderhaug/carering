(ns server.nlp)

(defn parse-message [{:keys [text] :as message}]
  (cond
    
    (or (clojure.string/starts-with? text "Hello")
        (clojure.string/starts-with? text "hello"))
    (assoc message :intent :nlp/greeting)

    :else message))
