(ns server.core
  (:require-macros
   [cljs.core.async.macros :as m
    :refer [go go-loop alt!]])
  (:require
   [polyfill.compat]
   [cljs.nodejs :as nodejs]
   [cljs.core.async :as async
    :refer [chan close! timeout put!]]
   [taoensso.timbre :as timbre]
   [reagent.core :as reagent
    :refer [atom]]
   [cljs-http.client :as http]
   [api.jokes :as jokes]
   [app.core :as app
    :refer [static-page]]
   [server.ringcentral :as ringcentral]
   [server.bot :as bot]))

(def express (nodejs/require "express"))
(def body-parser (nodejs/require "body-parser"))

(defn handler [req res]
  (if false #_(= "https" (aget (.-headers req) "x-forwarded-proto"))
    (.redirect res (str "http://" (.get req "Host") (.-url req)))
    (go
      (.set res "Content-Type" "text/html")
      (.send res (<! (static-page))))))

(defn dev-redirect [req res]
   (let [local (aget js/process "env" "REDIRECT")]
     (when-not (empty? local)
       (timbre/debug "REDIRECT:" (str local (.-url req)))
       (.redirect res 307 (str local (.-url req)))
       true)))

(defn wrap-dev [handler]
  (fn [req res]
    (or (dev-redirect req res)
        (handler req res))))

(defn api-handler [req res]
  (go-loop [in (jokes/resource-chan)
            [val ch] (alts! [in (timeout 5000)])]
    (if (identical? in ch)
      (do
        (.set res "Content-Type" "application/json")
        (.send res (clj->js val)))
      (do
        (.send (.status res 504) "Gateway Timeout")))))

(def oauth-handler
  (ringcentral/oauth-handler))

(def glip-callback
  (ringcentral/glip-handler bot/dispatch))

(def glip-webhook
  (ringcentral/glip-webhook))

(defn server [port success]
  (doto (express)
    (.use (.urlencoded body-parser #js {:extended false}))
    (.use (.json body-parser))
    (.get "/" (wrap-dev handler))
    (.get "/api" (wrap-dev api-handler))
    (.get "/oauth" (wrap-dev oauth-handler))
    (.post "/glip/callback" (wrap-dev glip-callback))
    (.post "/glip/webhook" glip-webhook) ;; cannot redirect to dev
    (.use (.static express "resources/public"))
    (.listen port success)))

(defn -main [& mess]
  (let [port (or (.-PORT (.-env js/process)) 1337)]
    (server port
            #(timbre/info (str "Server running at http://127.0.0.1:" port "/")))))

(set! *main-cli-fn* -main)
