# CareRing

Glip chatbot providing a medical office assistant integrating functionality from RingCentral.

[ClojureScript](https://clojurescript.org/) sources for the bot and server can be found in the [src/node](src/node/) directory.

## Deployment Instructions

Deploy by setting up a server on Heroku, configure the RingCentral developer portal, then add the bot to Glip.

### Requirements

git, heroku cli

    git clone https://norderhaug@bitbucket.org/norderhaug/carering.git
    cd carering

### Heroku Build

Start a server on Heroku:

    heroku apps:create
    git push heroku master
    heroku open

This will open a web page in your browser, validating that the server is up running.

### Glip Setup

* Sign in to the [RingCentral Developer Portal](https://developer.ringcentral.com/)
  with your account login and password.
  If you do not have RingCentral account, please sign up.

* In the [My Apps](https://developer.ringcentral.com/my-account.html#/applications)
  section of your account, create a new app
  with "Server/Bot" as Platform Type.

* For Oauth Settings, Set permissions to "Glip" and "Webhook subscriptions". Also add permissions for Contacts, Faxes, Read Contacts, SMS and others as needed.

* Set OAuth Redirect URI to the URI of the Heroku app with /oauth as path.

* Note the app id and secret provided by RingCentral.

* From a shell, add the app id, secret amnd oauth redirect to the Heroku environment:


    heroku config:add RC_CLIENT_ID=<client id>
    heroku config:add RC_CLIENT_SECRET=<client secret>
    heroku config:add RC_OAUTH_REDIRECT=<oauth redirect URI>

Note that the Oauth redirect URI has to be the same as configured for the bot on RingCentral.

### Run Demo

* Login to the [online portal](https://glip.devtest.ringcentral.com)
* Type the name of the bot into the Search field.

Note that you may have to log in again after re(adding) a bot to make it available.

## Development Workflow

Supports rapid interactive local development with REPL and source files hotloaded on changes, with the server redirecting request to ngrok for seamless workflow without changing the oauth configuration on RingCentral.

Assuming a `.env` file in the root of the repository containing:

    RC_CLIENT_ID=<client id>
    RC_CLIENT_SECRET=<client secret>

Start ngrok in a terminal:

    ngrok http 5000

Set the development server to redirect to the url provided by ngrok, e.g.:

    heroku config:add REDIRECT=http://264284f9.ngrok.io

Start figwheel for interactive development with
automatic builds and code loading:

    lein figwheel app server

Wait until Figwheel is ready to connect, then
start a server in another terminal:

    heroku local web

Open the displayed URL in a browser to verify it is running.
Figwheel will push code changes to the app and server.

After finishing development, remove the server redirect:

    heroku config:remove REDIRECT

## License

Copyright © 2018 Terje Norderhaug

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
