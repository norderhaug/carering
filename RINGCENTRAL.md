## REFERENCES

[Glip Developer Portal]
(https://developer.ringcentral.com/)

Online account portal for testing:
https://service.devtest.ringcentral.com

Support portal at:
https://app.glip.com/

HIPAA conduit:
https://service.devtest.ringcentral.com/tools/hipaa.html

## ISSUES

Here are notes about a few issues I encountered during development that may be of interest to RingCentral. Please feel free to ask for clarification.

1. Clicking the "Upgrade" button to get a free trial still insist on creating a new account instead of using the logged in sandbox account.

2. Example at <https://ringcentral-api-docs.readthedocs.io/en/latest/glip_bots/#create-a-simple-nodejs-application> suggests using a "private" app but elsewhere it says this may not support the used authenitification flow.

3. Need to sign out from glip.devtest.ringcentral.com
   in order for newly authorized bot to show up in the search for people.

4. documented explicit that /glip/persons/~ provides self as it is essential for chatbot development to avoid reprocessing messages from the bot.

5. event with ReadContacts permission set when attempting to access Get Contacts it reports "Error: In order to call this API endpoint, user needs to have [ReadPersonalContacts] permission for requested
resource." No ReadPersonalContacts permission is listed to be set in web interface. There is a Contacts permission though.

6. The login link on the plan page results in https://service.ringcentral.com/login/swAccessForbidden.html for my credentials.

7. Would be useful if there was a way to reauthentification from code. Had to repeatedly reset the oauth login manually by from the Bot tab of the RC developer, remove then add the bot.
